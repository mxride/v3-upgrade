
#!/bin/bash

check_exit_status() {

    if [ $? -eq 0 ]
    then
        echo
        echo "Success"
        echo
    else
        echo
        echo "[ERROR] Process Failed!"
        echo
		
        read -p "The last command exited with an error. Exit script? (yes/no) " answer

        if [ "$answer" == "yes" ]
        then
            exit 1
        fi
    fi
}

greeting() {

    echo
    echo "Hello, $USER. Let's update this system."
    echo
}


update() {

#    sudo apt-get update;
#    check_exit_status

#    sudo apt-get upgrade -y;
#    check_exit_status

#    sudo apt-get dist-upgrade -y;
#    check_exit_status

    sudo do-release-upgrade;
    check_exit_status
}

housekeeping() {

    sudo apt-get autoremove -y;
    check_exit_status

    sudo apt-get autoclean -y;
    check_exit_status

    sudo updatedb;
    check_exit_status

    crontab -u nodeware -r;
    check_exit_status

    sudo add-apt-repository --remove ppa:mrazavi/openvas;
    check_exit_status

    sudo rm /etc/apt/sources.list.d/mrazavi*;
    check_exit_status

    sudo apt purge openvas*;
    check_exit_status

    sudo rm -rf /home/nodeware/nodeware/;
    check_exit_status

    sudo rm -f /home/nodeware/update.py;
    check_exit_status
    sudo rm -f /home/nodeware/startup.py;
    check_exit_status
    sudo rm -f /home/nodeware/secring.gpg;
    check_exit_status
    sudo rm -f /home/nodeware/pubring.gpg;
    check_exit_status
    sudo rm -f /home/nodeware/omp.config;
    Check_exit_status


    sudo adduser config;
    check_exit_status
    sudo groupadd nodewarewrites;
    check_exit_status
    sudo usermod -a -G nodewarewrites config;
    check_exit_status
    sudo usermod -a -G nodewarewrites nodeware;
    check_exit_status

    sudo rm /var/log/nodeware/*;
    check_exit_status
    sudo chown nodeware:nodeware /var/log/nodeware;
    check_exit_status
    touch /var/log/nodeware/scanner.log;
    check_exit_status
    touch /var/log/nodeware/inventory.log;
    check_exit_status
    touch /var/log/nodeware/validate.log;
    check_exit_status
    touch /var/log/nodeware/system_profile.log;
    check_exit_status
    touch /var/log/nodeware/console.log;
    check_exit_status
    sudo chown -R nodeware:nodeware /var/log/nodeware/;
    check_exit_status


}

leave() {

    echo
    echo "--------------------"
    echo "- Update Complete! -"
    echo "--------------------"
    echo
    exit
}


greeting
update
housekeeping
leave
